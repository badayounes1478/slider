import React, { useEffect, useState } from 'react'

const App = () => {


  const [counter, setcounter] = useState(0)
  const [images] = useState([
    "https://images.unsplash.com/photo-1591021802314-173dba4030bf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=60",
    "https://images.unsplash.com/photo-1599713245275-355f0c71caed?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=60",
    "https://images.unsplash.com/photo-1599719840163-1cd5b7c1fabe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=60",
    "https://images.unsplash.com/photo-1599697686548-d39f7c80174c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=60"
  ])


  useEffect(() => {
    changeImage()
  }, [])

  const changeImage = () => {
    setInterval(() => {
      setcounter((prev) => {
        if (prev === 3) {
          return 0
        } else {
          return prev = prev + 1
        }
      })
    }, 2000);
  }


  return (
    <div style={{ backgroundImage: `url(${images[counter]})`, width: '100%', height: 600, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }}>
    </div>
  )
}

export default App
